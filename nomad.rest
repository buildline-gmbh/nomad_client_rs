@baseUrl = http://localhost:4646/v1

#################################################################
##
##  Nodes
##
#################################################################

@token =

### List Tokens
GET  {{baseUrl}}/acl/tokens
X-Nomad-Token: {{token}}

### Read self token
GET  {{baseUrl}}/acl/token/self
X-Nomad-Token: {{token}}

### Create Token
POST  {{baseUrl}}/acl/token
X-Nomad-Token: {{token}}

{
  "Name":"Test-Token2",
  "type":"client",
  "Policies":[""],
  "Global":false
}

### Update Token

@accessor_id =

POST  {{baseUrl}}/acl/token/{{accessor_id}}
X-Nomad-Token: {{token}}

{
  "AccessorID": "{{accessor_id}}",
  "Name": "Test-Token",
  "Type": "management"
}

#################################################################
##
##  Nodes
##
#################################################################

### Get Nodes
GET  {{baseUrl}}/nodes
X-Nomad-Token: cc5808b1-4db0-f3ea-33a9-25af117a1559


#################################################################
##
##  Status
##
#################################################################

### Get Leader
GET  {{baseUrl}}/status/leader


### Get peers
GET  {{baseUrl}}/status/peers


#################################################################
##
##  Namespace
##
#################################################################

### Get Namespaces
GET  {{baseUrl}}/namespaces

### Create Namespace
POST {{baseUrl}}/namespace

{
  "Name": "api-prod"
}


### Delete Namespace
@namespaceName = api-prod

DELETE {{baseUrl}}/namespace/{{namespaceName}}

#################################################################
##
##  Jobs
##
#################################################################

### parse job (hcl -> json)
# jq -Rsc '{ JobHCL: ., Canonicalize: true }' example.nomad > payload.json

POST {{baseUrl}}/jobs/parse

{
  "JobHCL": "job \"exitWith\" {\n  type        = \"batch\"\n  datacenters = [\"dc1\"]\n\n  parameterized {\n    meta_required = [\"EXITCODE\"]\n    meta_optional = []\n  }\n\n  group \"default\" {\n\n    reschedule {\n      attempts  = 0\n      unlimited = false\n    }\n\n    restart {\n      attempts = 1\n      mode     = \"fail\"\n    }\n\n    task \"main\" {\n      driver = \"docker\"\n      \n      config {\n        image   = \"alpine:latest\"\n        command = \"sh\"\n        args = [\n          \"-c\",\n          \"exit ${NOMAD_META_EXITCODE}\"\n        ]\n      }\n    }\n  }\n}\n",
  "Canonicalize": true
}


### create job

POST {{baseUrl}}/jobs

{
  "Job": {
    "Datacenters": ["dc1"],
    "Namespace": "api-prod",
    "ID": "exitCode",
    "TaskGroups": [
      {
        "Name": "default",
        "Tasks": [
          {
            "Name": "main",
            "Config": {
              "image": "redis:7",
              "ports": ["db"]
            },
            "Driver": "docker" 
          }
        ]
      }
    ]
  }
}


### Get Jobs
GET  {{baseUrl}}/jobs


### Get Jobs with Filter
@filter=
GET  {{baseUrl}}/jobs?filter=

### Get Job
@jobId = exitWith2
GET  {{baseUrl}}/job/{{jobId}}


### Get Job Summary
GET  {{baseUrl}}/job/{{jobId}}/summary

### Dispatch Job

POST {{baseUrl}}/job/{{jobId}}/dispatch

{
  "Payload": "",
  "Meta": {
    "ExitWith": "0"
  }
}

### Stop/Delete Job
@jobToDelete = exitWith/dispatch-1669993232-a50fa2e6
DELETE  {{baseUrl}}/job/{{jobToDelete}}?purge=true


#################################################################
##
##  Allocations
##
#################################################################

### Get Job Allocations
@jobIdWithAlloc = exitWith/dispatch-1674561068-dd8a9d33
GET  {{baseUrl}}/job/{{jobIdWithAlloc}}/allocations



@allocId = 3bb1ef7e-3d7e-4d5b-8379-db33355b0754

### Get Allocation
GET {{baseUrl}}/allocation/{{allocId}}

### Stop Allocation
POST  {{baseUrl}}/allocation/{{allocId}}/stop


### Restart Allocation
POST  {{baseUrl}}/allocation/{{allocId}}/restart

{
  "TaskName": "main",
  "AllTasks": false
}

### Signal Allocation
# If Task is omitted, the signal will be sent to all tasks in the allocation.
POST  {{baseUrl}}/allocation/{{allocId}}/signal

{
  "Signal": "SIGUSR1",
  "Task": "main"
}

### Restart Allocation
POST  {{baseUrl}}/client/allocation/{{allocId}}/restart

#################################################################
##
##  Clients
##
#################################################################

### Get Client Stats
GET {{baseUrl}}/client/stats


### List Files
@allocationWithFiles = 3d93c4ac-5c3a-6ac6-33df-d9a4c3fe2a9c

GET {{baseUrl}}/client/fs/ls/{{allocationWithFiles}}

### List Files with path
GET {{baseUrl}}/client/fs/ls/{{allocationWithFiles}}?path=alloc/logs

### Get File
GET {{baseUrl}}/client/fs/cat/{{allocationWithFiles}}?path=alloc/logs/main.stdout.0

### Stats File
GET {{baseUrl}}/client/fs/stat/{{allocationWithFiles}}?path=alloc/logs/main.stdout.0

### Stream File
GET {{baseUrl}}/client/fs/stream/{{allocationWithFiles}}?path=alloc/logs/main.stdout.0

### Force Allocation Garbage Collection
GET {{baseUrl}}/client/allocation/{{allocationWithFiles}}/gc

### Read Metadata
GET {{baseUrl}}/client/metadata?node_id=bdd27920-bb18-9c72-fbef-4eb25d1588d4

### Update Metadata
POST {{baseUrl}}/client/metadata

{
  "nodeId":null,
  "meta": {
    "foo": "bla22"
  }
}

#################################################################
##
##  Plugins
##
#################################################################

### Get Plugins
GET {{baseUrl}}/plugins?type=driver

#################################################################
##
##  Variables
##
#################################################################

### List Variables
GET {{baseUrl}}/vars

@var_path = test

### Get Variable
GET {{baseUrl}}/var/{{var_path}}

### Upsert Variable
PUT {{baseUrl}}/var/{{var_path}}

{
  "Namespace": "default",
  "Path": "test",
  "Items": {
    "test_key": "bl1"
  }
}