use reqwest::Method;

use crate::api::namespace::models::{NamespaceListParams, NamespaceRequest};
use crate::models::namespace::Namespace;
use crate::{ClientError, NomadClient};

impl NomadClient {
    pub async fn namespace_list(
        &self,
        params: Option<NamespaceListParams>,
    ) -> Result<Vec<Namespace>, ClientError> {
        let req = self.request(Method::GET, "/namespaces").query(&params);

        self.send::<Vec<Namespace>>(req).await
    }

    pub async fn namespace_get(&self, namespace: &str) -> Result<Namespace, ClientError> {
        let req = self.request(Method::GET, &format!("/namespace/{}", namespace));

        self.send::<Namespace>(req).await
    }

    pub async fn namespace_create(&self, namespace: &NamespaceRequest) -> Result<(), ClientError> {
        let req = self.request(Method::POST, "/namespace").json(namespace);

        self.send_plain(req).await.map(|_| ())
    }

    pub async fn namespace_delete(&self, namespace: &str) -> Result<(), ClientError> {
        let req = self.request(Method::DELETE, &format!("/namespace/{}", namespace));

        self.send_plain(req).await.map(|_| ())
    }
}

pub mod models {
    use std::collections::HashMap;

    use serde::Serialize;

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "PascalCase")]
    pub struct NamespaceRequest {
        pub name: String,
        pub description: Option<String>,
        pub meta: Option<HashMap<String, String>>,
    }

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct NamespaceListParams {
        pub prefix: String,
    }
}

#[cfg(test)]
mod tests {
    use crate::api::namespace::models::{NamespaceListParams, NamespaceRequest};
    use crate::NomadClient;

    #[tokio::test]
    async fn namespace_list_should_return_list_of_namespaces() {
        let client = NomadClient::default();

        match client.namespace_list(None).await {
            Ok(v) => assert!(!v.is_empty()),
            Err(e) => panic!("{:#?}", e),
        }
    }

    #[tokio::test]
    async fn namespace_list_should_return_matching_namespaces_when_params_given() {
        let client = NomadClient::default();
        let param = NamespaceListParams {
            prefix: "unknown".into(),
        };

        match client.namespace_list(Some(param)).await {
            Ok(v) => assert!(v.is_empty()),
            Err(e) => panic!("{:#?}", e),
        }
    }

    #[tokio::test]
    async fn namespace_get_should_return_namespace() {
        let client = NomadClient::default();

        match client.namespace_get("default").await {
            Ok(v) => assert_eq!(v.name, Some("default".to_string())),
            Err(e) => panic!("{:#?}", e),
        }
    }

    #[tokio::test]
    async fn namespace_create_should_create_namespace() {
        let client = NomadClient::default();
        let req = NamespaceRequest {
            name: "example".into(),
            meta: None,
            description: None,
        };

        match client.namespace_create(&req).await {
            Ok(_) => assert!(true),
            Err(e) => panic!("{:#?}", e),
        }
    }

    #[tokio::test]
    async fn namespace_delete_should_delete_namespace() {
        let client = NomadClient::default();
        let req = NamespaceRequest {
            name: "example".into(),
            meta: None,
            description: None,
        };

        let _ = client.namespace_create(&req).await;

        match client.namespace_delete("example").await {
            Ok(_) => assert!(true),
            Err(e) => panic!("{:#?}", e),
        }
    }
}
