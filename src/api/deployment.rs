use reqwest::Method;

use crate::api::deployment::models::{DeploymentFailResponse, DeploymentListParams};
use crate::models::allocation::Allocation;
use crate::models::deployment::Deployment;
use crate::{ClientError, NomadClient};

impl NomadClient {
    pub async fn deployment_list(
        &self,
        params: Option<DeploymentListParams>,
    ) -> Result<Vec<Deployment>, ClientError> {
        let req = self.request(Method::GET, "/deployments").query(&params);

        self.send::<Vec<Deployment>>(req).await
    }

    pub async fn deployment_get(&self, id: &str) -> Result<Deployment, ClientError> {
        let req = self.request(Method::GET, &format!("/deployment/{}", id));

        self.send::<Deployment>(req).await
    }

    pub async fn deployment_allocation_list(
        &self,
        id: &str,
    ) -> Result<Vec<Allocation>, ClientError> {
        let req = self.request(Method::GET, &format!("/deployment/allocations/{}", id));

        self.send::<Vec<Allocation>>(req).await
    }

    pub async fn deployment_fail(&self, id: &str) -> Result<DeploymentFailResponse, ClientError> {
        let req = self.request(Method::POST, &format!("/deployment/fail/{}", id));

        self.send::<DeploymentFailResponse>(req).await
    }
}

pub mod models {
    use serde::{Deserialize, Serialize};

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct DeploymentListParams {
        pub prefix: Option<String>,
        pub namespace: Option<String>,
        pub next_token: Option<String>,
        pub per_page: Option<i32>,
        pub filter: Option<String>,
        pub reverse: Option<bool>,
    }

    #[derive(Debug, Deserialize)]
    #[serde(rename_all = "PascalCase")]
    pub struct DeploymentFailResponse {
        #[serde(rename = "EvalID")]
        pub eval_id: String,
        pub eval_create_index: i32,
        pub deployment_modify_index: i32,
        pub reverted_job_version: i32,
        pub index: i32,
    }

    #[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
    pub struct DeploymentAllocHealthRequest {
        #[serde(rename = "DeploymentID", skip_serializing_if = "Option::is_none")]
        pub deployment_id: Option<String>,
        #[serde(
            rename = "HealthyAllocationIDs",
            skip_serializing_if = "Option::is_none"
        )]
        pub healthy_allocation_ids: Option<Vec<String>>,
        #[serde(rename = "Namespace", skip_serializing_if = "Option::is_none")]
        pub namespace: Option<String>,
        #[serde(rename = "Region", skip_serializing_if = "Option::is_none")]
        pub region: Option<String>,
        #[serde(rename = "SecretID", skip_serializing_if = "Option::is_none")]
        pub secret_id: Option<String>,
        #[serde(
            rename = "UnhealthyAllocationIDs",
            skip_serializing_if = "Option::is_none"
        )]
        pub unhealthy_allocation_ids: Option<Vec<String>>,
    }

    #[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
    pub struct DeploymentPauseRequest {
        #[serde(rename = "DeploymentID", skip_serializing_if = "Option::is_none")]
        pub deployment_id: Option<String>,
        #[serde(rename = "Namespace", skip_serializing_if = "Option::is_none")]
        pub namespace: Option<String>,
        #[serde(rename = "Pause", skip_serializing_if = "Option::is_none")]
        pub pause: Option<bool>,
        #[serde(rename = "Region", skip_serializing_if = "Option::is_none")]
        pub region: Option<String>,
        #[serde(rename = "SecretID", skip_serializing_if = "Option::is_none")]
        pub secret_id: Option<String>,
    }

    #[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
    pub struct DeploymentPromoteRequest {
        #[serde(rename = "All", skip_serializing_if = "Option::is_none")]
        pub all: Option<bool>,
        #[serde(rename = "DeploymentID", skip_serializing_if = "Option::is_none")]
        pub deployment_id: Option<String>,
        #[serde(rename = "Groups", skip_serializing_if = "Option::is_none")]
        pub groups: Option<Vec<String>>,
        #[serde(rename = "Namespace", skip_serializing_if = "Option::is_none")]
        pub namespace: Option<String>,
        #[serde(rename = "Region", skip_serializing_if = "Option::is_none")]
        pub region: Option<String>,
        #[serde(rename = "SecretID", skip_serializing_if = "Option::is_none")]
        pub secret_id: Option<String>,
    }

    #[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
    pub struct DeploymentUnblockRequest {
        #[serde(rename = "DeploymentID", skip_serializing_if = "Option::is_none")]
        pub deployment_id: Option<String>,
        #[serde(rename = "Namespace", skip_serializing_if = "Option::is_none")]
        pub namespace: Option<String>,
        #[serde(rename = "Region", skip_serializing_if = "Option::is_none")]
        pub region: Option<String>,
        #[serde(rename = "SecretID", skip_serializing_if = "Option::is_none")]
        pub secret_id: Option<String>,
    }

    #[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
    pub struct DeploymentUpdateResponse {
        #[serde(
            rename = "DeploymentModifyIndex",
            skip_serializing_if = "Option::is_none"
        )]
        pub deployment_modify_index: Option<i32>,
        #[serde(rename = "EvalCreateIndex", skip_serializing_if = "Option::is_none")]
        pub eval_create_index: Option<i32>,
        #[serde(rename = "EvalID", skip_serializing_if = "Option::is_none")]
        pub eval_id: Option<String>,
        #[serde(rename = "LastIndex", skip_serializing_if = "Option::is_none")]
        pub last_index: Option<i32>,
        #[serde(rename = "RequestTime", skip_serializing_if = "Option::is_none")]
        pub request_time: Option<i64>,
        #[serde(rename = "RevertedJobVersion", skip_serializing_if = "Option::is_none")]
        pub reverted_job_version: Option<i32>,
    }
}
