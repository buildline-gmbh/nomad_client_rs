use reqwest::Method;

use crate::{ClientError, NomadClient};

impl NomadClient {
    pub async fn status_get_leader(&self) -> Result<String, ClientError> {
        let req = self.request(Method::GET, "/status/leader");

        self.send::<String>(req).await
    }

    pub async fn status_get_peers(&self) -> Result<Vec<String>, ClientError> {
        let req = self.request(Method::GET, "/status/peers");

        self.send::<Vec<String>>(req).await
    }
}

#[cfg(test)]
mod tests {
    use std::net::SocketAddr;

    use crate::{Config, MTLSConfig, NomadClient};

    #[tokio::test]
    async fn status_get_leader_should_return_socket_address_of_leader() {
        let client = NomadClient::default();

        match client.status_get_leader().await {
            Ok(url_string) => match url_string.parse::<SocketAddr>() {
                Ok(_) => assert!(true),
                Err(e) => panic!("{:#?}", e),
            },

            Err(e) => panic!("{:#?}", e),
        }
    }

    #[tokio::test]
    async fn status_get_peers_should_return_socket_address_of_peers() {
        let client = NomadClient::default();

        match client.status_get_peers().await {
            Ok(peers) => assert!(!peers.is_empty()),
            Err(e) => panic!("{:#?}", e),
        }
    }

    /*    #[tokio::test]
    async fn mtls_test() {
        let mut config = Config::default();
        config.base_url = "https://localhost".into();
        config.mtls = Some(MTLSConfig::new("cli.pem","/cli-key.pem"));

        let client = NomadClient::new(config);

        match client.status_get_peers().await {
            Ok(peers) => assert!(!peers.is_empty()),
            Err(e) => panic!("{:#?}", e),
        }
    }*/
}
