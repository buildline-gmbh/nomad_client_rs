use reqwest::Method;

use crate::api::path_combine;
use crate::api::variable::models::{
    VariableCreateRequest, VariableDeleteParams, VariableGetParams, VariableListParams,
};
use crate::models::variable::Variable;
use crate::{ClientError, NomadClient};

impl NomadClient {
    pub async fn variables_list(
        &self,
        params: &VariableListParams,
    ) -> Result<Vec<Variable>, ClientError> {
        let req = self.request(Method::GET, "/vars").query(&params);

        self.send::<Vec<Variable>>(req).await
    }

    pub async fn variable_get(
        &self,
        var_path: &str,
        params: &VariableGetParams,
    ) -> Result<Variable, ClientError> {
        let req = self
            .request(Method::GET, &path_combine("/var", var_path))
            .query(&params);

        self.send::<Variable>(req).await
    }

    pub async fn variable_create(
        &self,
        var_path: &str,
        req: &VariableCreateRequest,
    ) -> Result<Variable, ClientError> {
        let req = self
            .request(Method::PUT, &path_combine("/var", var_path))
            .json(&req);

        self.send::<Variable>(req).await
    }

    pub async fn variable_delete(
        &self,
        var_path: &str,
        params: &VariableDeleteParams,
    ) -> Result<(), ClientError> {
        let req = self
            .request(Method::DELETE, &path_combine("/var", var_path))
            .query(&params);

        self.send_plain(req).await.map(|_| ())
    }
}

pub mod models {
    use std::collections::HashMap;

    use serde::Serialize;

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct VariableListParams {
        pub prefix: Option<String>,
        #[serde(rename = "next_token")]
        pub next_token: Option<String>,
        #[serde(rename = "per_page")]
        pub per_page: Option<u64>,
        pub filter: Option<String>,
        pub namespace: Option<String>,
    }

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct VariableGetParams {
        pub namespace: Option<String>,
    }

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "PascalCase")]
    pub struct VariableCreateRequest {
        pub namespace: String,
        pub path: String,
        pub items: HashMap<String, String>,
    }

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct VariableDeleteParams {
        pub namespace: Option<String>,
    }
}

#[cfg(test)]
mod tests {
    use crate::api::namespace::models::{NamespaceListParams, NamespaceRequest};
    use crate::api::variable::models::{VariableCreateRequest, VariableDeleteParams};
    use crate::models::variable::Variable;
    use crate::{ClientError, NomadClient};

    #[tokio::test]
    async fn variable_create() {
        let client = NomadClient::default();

        let mut request = VariableCreateRequest::default();
        request.items.insert("key".to_string(), "value".to_string());

        match client.variable_create("testVariable", &request).await {
            Ok(_) => assert!(true),
            Err(err) => panic!("{err}"),
        }
    }

    #[tokio::test]
    async fn variable_delete_should_return_void() {
        let client = NomadClient::default();

        match client
            .variable_delete("thisDoesNotExist", &VariableDeleteParams::default())
            .await
        {
            Ok(v) => assert!(true),
            Err(e) => panic!("{:#?}", e),
        }

        match client
            .variable_delete("testVariable", &VariableDeleteParams::default())
            .await
        {
            Ok(_) => assert!(true),
            Err(err) => panic!("{err}"),
        }
    }
}
