use reqwest::Method;

use crate::api::service::models::{NamespacedService, ServiceParams};
use crate::models::ServiceRegistration;
use crate::{ClientError, NomadClient};

impl NomadClient {
    pub async fn service_get(
        &self,
        service_name: &str,
        params: &ServiceParams,
    ) -> Result<Vec<ServiceRegistration>, ClientError> {
        let req = self
            .request(Method::GET, &format!("/service/{}", service_name))
            .query(params);

        self.send::<Vec<ServiceRegistration>>(req).await
    }

    pub async fn service_list(
        &self,
        params: &ServiceParams,
    ) -> Result<Vec<NamespacedService>, ClientError> {
        let req = self.request(Method::GET, "/services").query(params);

        self.send::<Vec<NamespacedService>>(req).await
    }
}

pub mod models {
    use serde::{Deserialize, Serialize};

    use crate::models::ServiceRegistration;

    #[derive(Clone, Debug, Default, Deserialize, Serialize)]
    #[serde(rename_all = "PascalCase")]
    pub struct NamespacedService {
        pub namespace: Option<String>,
        pub services: Vec<ServiceRegistration>,
    }

    #[derive(Debug, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct ServiceParams {
        pub namespace: Option<String>,
    }

    impl Default for ServiceParams {
        fn default() -> Self {
            ServiceParams {
                namespace: Some("default".to_string()),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::api::service::models::ServiceParams;
    use crate::NomadClient;

    #[tokio::test]
    #[ignore]
    async fn test() {
        let client = NomadClient::default();
        let result = client.service_list(&ServiceParams::default()).await;

        println!("{result:#?}")
    }

    #[tokio::test]
    #[ignore]
    async fn test2() {
        let client = NomadClient::default();
        let result = client
            .service_get("nats-leaf-node", &ServiceParams::default())
            .await;

        println!("{result:#?}")
    }
}
