pub mod acl;
pub mod allocation;
pub mod client;
pub mod deployment;
pub mod event;
pub mod job;
pub mod namespace;
pub mod service;
pub mod status;
pub mod variable;

pub fn path_combine(a: &str, b: &str) -> String {
    let a = a.trim_end_matches('/');
    let b = b.trim_start_matches('/');
    format!("{}/{}", a, b)
}

#[cfg(test)]
mod tests {
    use crate::api::path_combine;

    #[test]
    fn path_combine_should_return_valid_path() {
        assert_eq!("/a/b", path_combine("/a", "/b"));
        assert_eq!("/a/b", path_combine("/a/", "/b"));
        assert_eq!("/a/b", path_combine("/a", "b"));
        assert_eq!("a/b", path_combine("a", "b"));
        assert_eq!("a/b", path_combine("a", "/b"));
    }
}
