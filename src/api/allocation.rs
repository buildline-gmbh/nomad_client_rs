use reqwest::Method;

use crate::api::allocation::models::AllocationRestartRequest;
use crate::{ClientError, NomadClient};

impl NomadClient {
    pub async fn allocation_restart(
        &self,
        alloc_id: &str,
        req: &AllocationRestartRequest,
    ) -> Result<(), ClientError> {
        let req = self
            .request(
                Method::POST,
                &format!("/client/allocation/{}/restart", alloc_id),
            )
            .json(req);

        self.send::<()>(req).await
    }
}

pub mod models {
    use serde::{Deserialize, Serialize};

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct AllocationRestartRequest {
        pub task_name: Option<String>,
        pub all_tasks: Option<bool>,
    }

    #[derive(Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
    pub struct AllocStopResponse {
        #[serde(rename = "EvalID", skip_serializing_if = "Option::is_none")]
        pub eval_id: Option<String>,
        #[serde(rename = "Index", skip_serializing_if = "Option::is_none")]
        pub index: Option<i32>,
    }
}
