use reqwest::Method;

use crate::api::acl::models::{AclTokenCreateRequest, AclTokenUpdateRequest, AclTokensListParams};
use crate::models::acl_token::AclToken;
use crate::{ClientError, NomadClient};

impl NomadClient {
    pub async fn acl_tokens_list(
        &self,
        params: &AclTokensListParams,
    ) -> Result<Vec<AclToken>, ClientError> {
        let req = self.request(Method::GET, "/acl/tokens").query(params);

        self.send::<Vec<AclToken>>(req).await
    }

    pub async fn acl_token_create(
        &self,
        req: &AclTokenCreateRequest,
    ) -> Result<AclToken, ClientError> {
        let req = self.request(Method::POST, "/acl/token").json(req);

        self.send::<AclToken>(req).await
    }

    pub async fn acl_token_update(
        &self,
        accessor_id: String,
        req: &AclTokenUpdateRequest,
    ) -> Result<AclToken, ClientError> {
        let req = self
            .request(Method::POST, &format!("/acl/token/{}", accessor_id))
            .json(req);

        self.send::<AclToken>(req).await
    }

    pub async fn acl_token_read(&self, accessor_id: String) -> Result<AclToken, ClientError> {
        let req = self.request(Method::GET, &format!("/acl/token/{}", accessor_id));

        self.send::<AclToken>(req).await
    }

    pub async fn acl_token_read_self(&self) -> Result<AclToken, ClientError> {
        let req = self.request(Method::GET, "/acl/token/self");

        self.send::<AclToken>(req).await
    }

    pub async fn acl_token_delete(&self, accessor_id: String) -> Result<(), ClientError> {
        let req = self.request(Method::DELETE, &format!("/acl/token/{}", accessor_id));

        self.send_plain(req).await.map(|_| ())
    }
}

pub mod models {
    use serde::Serialize;

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct AclTokensListParams {
        pub global: Option<bool>,
        pub prefix: Option<String>,
        #[serde(rename = "next_token")]
        pub next_token: Option<String>,
        #[serde(rename = "per_page")]
        pub per_page: Option<u64>,
        pub filter: Option<String>,
        pub reverse: Option<bool>,
    }

    #[derive(Debug, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct AclTokenCreateRequest {
        pub name: Option<String>,
        pub r#type: AclTokenType,
        pub policies: Vec<String>,
        pub global: bool,
        pub expiration_time: Option<String>,
        pub expiration_ttl: Option<i64>,
    }

    #[derive(Debug, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct AclTokenUpdateRequest {
        #[serde(rename = "AccessorID")]
        pub accessor_id: String,
        pub name: Option<String>,
        pub r#type: AclTokenType,
        pub policies: Vec<String>,
    }

    #[derive(Clone, Debug, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub enum AclTokenType {
        Client,
        Management,
    }
}
