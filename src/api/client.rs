use reqwest::Method;

use crate::api::client::models::{
    ClientMetadataResponse, ClientReadMetadataParams, ClientUpdateMetadataRequest,
};
use crate::models::alloc_file_info::AllocFileInfo;
use crate::{ClientError, NomadClient};

impl NomadClient {
    pub async fn client_list_files(
        &self,
        alloc_id: &str,
        path: Option<String>,
    ) -> Result<Vec<AllocFileInfo>, ClientError> {
        let req = self.request(
            Method::GET,
            &format!(
                "/client/fs/ls/{}?path={}",
                alloc_id,
                path.unwrap_or("/".into())
            ),
        );

        self.send::<Vec<AllocFileInfo>>(req).await
    }

    pub async fn client_get_file(
        &self,
        alloc_id: &str,
        file_path: &str,
    ) -> Result<String, ClientError> {
        let req = self.request(
            Method::GET,
            &format!("/client/fs/cat/{}?path={}", alloc_id, file_path),
        );

        self.send_plain(req).await
    }

    pub async fn client_get_file_info(
        &self,
        alloc_id: &str,
        file_path: &str,
    ) -> Result<AllocFileInfo, ClientError> {
        let req = self.request(
            Method::GET,
            &format!("/client/fs/stat/{}?path={}", alloc_id, file_path),
        );

        self.send::<AllocFileInfo>(req).await
    }

    pub async fn client_read_metadata(
        &self,
        params: &ClientReadMetadataParams,
    ) -> Result<ClientMetadataResponse, ClientError> {
        let req = self.request(Method::GET, "/client/metadata").query(params);

        self.send::<ClientMetadataResponse>(req).await
    }

    pub async fn client_update_metadata(
        &self,
        req: &ClientUpdateMetadataRequest,
    ) -> Result<ClientMetadataResponse, ClientError> {
        let req = self.request(Method::POST, "/client/metadata").json(req);

        self.send::<ClientMetadataResponse>(req).await
    }
}

pub mod models {
    use std::collections::HashMap;

    use serde::{Deserialize, Serialize};

    #[derive(Debug, Deserialize)]
    #[serde(rename_all = "PascalCase")]
    pub struct ClientMetadataResponse {
        pub meta: Option<HashMap<String, Option<String>>>,
        pub dynamic: Option<HashMap<String, Option<String>>>,
        pub r#static: Option<HashMap<String, Option<String>>>,
    }

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct ClientReadMetadataParams {
        #[serde(rename = "node_id")]
        pub node_id: Option<String>,
    }

    #[derive(Debug, Default, Serialize)]
    #[serde(rename_all = "camelCase")]
    pub struct ClientUpdateMetadataRequest {
        pub node_id: Option<String>,
        pub meta: HashMap<String, Option<String>>,
    }
}

#[cfg(test)]
mod tests {
    // use std::collections::HashMap;
    //
    // use crate::api::client::models::{ClientReadMetadataParams, ClientUpdateMetadataRequest};
    // use crate::NomadClient;
    //
    // #[tokio::test]
    // pub async fn test_client_update_metadata() {
    //     let client = NomadClient::default();
    //
    //     let body = ClientUpdateMetadataRequest {
    //         meta: HashMap::from([("foo".to_string(), Some("bar".to_string()))]),
    //         ..ClientUpdateMetadataRequest::default()
    //     };
    //
    //     match client.client_update_metadata(&body).await {
    //         Ok(resp) => {
    //             println!("{:#?}", resp);
    //         }
    //         Err(e) => panic!("{:#?}", e),
    //     }
    // }
    //
    // #[tokio::test]
    // pub async fn test_client_read_metadata() {
    //     let client = NomadClient::default();
    //
    //     let params = ClientReadMetadataParams {
    //         ..ClientReadMetadataParams::default()
    //     };
    //
    //     match client.client_read_metadata(&params).await {
    //         Ok(resp) => {
    //             println!("{:#?}", resp);
    //         }
    //         Err(e) => panic!("{:#?}", e),
    //     }
    // }
}
