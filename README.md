# Nomad Client

[![License](https://img.shields.io/crates/l/nomad-client-rs)](https://gitlab.com/buildline-gmbh/nomad_client_rs)
[![Lines of code](https://tokei.rs/b1/gitlab/buildline-gmbh/nomad_client_rs?category=code)](https://gitlab.com/buildline-gmbh/nomad_client_rs)
[![Crates.io](https://img.shields.io/crates/v/nomad-client-rs)](https://crates.io/crates/nomad-client-rs)
[![Crates.io](https://img.shields.io/crates/d/nomad-client-rs)](https://crates.io/crates/nomad-client-rs)

Nomad HTTP API Client

## Table of contents

- [Overview](#overview)
- [Quick start](#quick-start)
- [Usage](#usage)
    - [Implemented API Endpoints](#implemented-api-endpoints)
- [Environment](#environment)
    - [Variables](#variables)

## Overview

[HTTP API Version](https://developer.hashicorp.com/nomad/api-docs) : v1.5.x

## Quick start

Install the package over cli

```bash
cargo add nomad-client-rs
```

or add the following to your `Cargo.toml`

```toml
[dependencies]
nomad-client-rs = "x.x.x"
```

## Usage

Default nomad client uses [environment variables / default values](#variables)

```rust
let client = NomadClient::default();
```

Nomad client with custom config

```rust
let client_config = Config {
  base_url: "http://example".into(),
  port: 1234,
  api_version: "v1".into(),
  token: None,
  ..Config::default()
};

let client = NomadClient::new(client_config);
```

### Implemented API Endpoints

| Class      | Method                     | HTTP Request                                    |
|------------|----------------------------|-------------------------------------------------|
| allocation | allocation_restart         | POST /client/allocation/{alloc_id}/restart      |
| acl        | acl_tokens_list            | GET /acl/tokens                                 |
| acl        | acl_token_create           | POST /acl/token                                 |
| acl        | acl_token_update           | POST /acl/token/{accessor_id}                   |
| acl        | acl_token_read             | GET /acl/token/{accessor_id}                    |
| acl        | acl_token_read_self        | GET /acl/token/self                             |
| acl        | acl_token_delete           | DELETE /acl/token/{accessor_id}                 |
| client     | client_list_files          | GET /client/fs/ls/{alloc_id}?path={file_path}   |
| client     | client_get_file            | GET /client/fs/cat/{alloc_id}?path={file_path}  |
| client     | client_get_file_info       | GET /client/fs/stat/{alloc_id}?path={file_path} |
| client     | client_read_metadata       | GET /client/metadata                            |
| client     | client_update_metadata     | POST /client/metadata                           |
| deployment | deployment_list            | GET /deployments                                |
| deployment | deployment_get             | GET /deployments/{id}                           |
| deployment | deployment_allocation_list | GET /deployment/allocations/{id}                |
| deployment | deployment_fail            | POST /deployment/fail/{id}                      |
| event      | events_subscribe           | GET /event/stream                               |         
| job        | job_dispatch               | POST /job/{job_name}/dispatch                   |
| job        | job_parse                  | POST /job/parse                                 |
| job        | job_create                 | POST /jobs                                      |
| job        | job_list_allocations       | GET /job/{job_name}/allocations                 |
| job        | job_delete                 | DELETE /job/{job_name}                          |
| namespace  | namespace_list             | GET /namespaces                                 |
| namespace  | namespace_get              | GET /namespace/{namespace}                      |
| namespace  | namespace_create           | POST /namespace                                 |
| namespace  | namespace_delete           | DELETE /namespace/{namespace}                   |
| status     | status_get_peers           | GET /status/peers                               |
| status     | status_get_leader          | GET /status/leader                              |
| variable   | variables_list             | GET /vars                                       |
| variable   | variable_get               | GET /var/{var_path}                             |
| variable   | variable_create            | PUT /var/{var_path}                             |
| variable   | variable_delete            | DELETE /var/{var_path}                          |
| service    | service_list               | GET /services                                   |
| service    | variable_get               | GET /services/{service_name}                    |

## Environment

### Variables

NomadClient can be configured by env variables. <br/>
If env variable is not available it will use specified default value.

| Variable                 | Default          |
|--------------------------|------------------|
| NOMAD_BASE_URL           | http://localhost |
| NOMAD_PORT               | 4646             |
| NOMAD_API_VERSION        | v1               |
| NOMAD_SECRET_TOKEN       |                  |
| NOMAD_TLS_ALLOW_INSECURE | false            |
| NOMAD_MTLS_CERT_PATH     |                  |
| NOMAD_MTLS_KEY_PATH      |                  |

## Authentiction

### MTLS

NomadClient supports mtls authentication using `.pem` files only.
To use mtls authentication simply configure `mtls` field in `Config` struct.

```rust
let client_config = Config {
  mtls: Some(MTLSConfig::new("path/to/client.pem", "path/to/client-key.pem"));
  ..Config::default()
};
```

